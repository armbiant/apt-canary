#!/bin/sh

# Copyright (C) 2023 Simon Josefsson <simon@josefsson.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

AT="$@"

eval $(apt-config shell BASE_URL Canary::Base-URL)
if test -z "$BASE_URL"; then
    logger --tag apt-canary "witness URL not configured"
else
    # Below we use the fact that the final two parameters are
    # signfile/datafile.  Note that for InRelease files, we still get
    # two different files but they are created from InRelease by apt.
    #
    # https://salsa.debian.org/apt-team/apt/-/blob/09d4933fa8a472ea3f1efa9484f962b94886c07f/apt-pkg/indexcopy.cc#L540
    # https://salsa.debian.org/apt-team/apt/-/blob/09d4933fa8a472ea3f1efa9484f962b94886c07f/apt-pkg/contrib/gpgv.cc#L113
    # https://salsa.debian.org/apt-team/apt/-/blob/09d4933fa8a472ea3f1efa9484f962b94886c07f/apt-pkg/contrib/gpgv.cc#L418
    # https://salsa.debian.org/apt-team/apt/-/blob/09d4933fa8a472ea3f1efa9484f962b94886c07f/cmdline/apt-key.in#L800
    datafile=""
    for p in $AT; do
	signfile=$datafile
	datafile=$p
    done

    SHA256=`cat $datafile | sha256sum - | sed 's,  -,,'`
    URL="$BASE_URL/$SHA256.witness"

    if wget -q -O- $URL | grep "^Canary: $URL$" > /dev/null; then
	logger --tag apt-canary "successful witness $URL"
    else
	logger --tag apt-canary "unable to find successful witness $URL"

	cat $datafile | logger --tag apt-canary-datafile
	cat $signfile | logger --tag apt-canary-signfile

	exit 1
    fi
fi

exit 0
